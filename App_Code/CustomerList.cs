﻿using System.Collections.Generic;
using System.Web;

/// <summary>
/// This class represents a collection of Customers.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// Febuary 5, 2015 | Spring
/// </version>
public class CustomerList
{
    /// <summary>
    /// The cart items
    /// </summary>
    private readonly List<Customer> _customers;

    /// <summary>
    /// Initializes a new instance of the <see cref="CustomerList" /> class.
    /// </summary>
    public CustomerList()
    {
        this._customers = new List<Customer>();
    }

    /// <summary>
    /// Gets the count.
    /// </summary>
    /// <value>
    /// The count.
    /// </value>
    public int Count
    {
        get
        {
            return this._customers.Count;
        }
    }

    /// <summary>
    /// Gets or sets the <see cref="Customer" /> at the specified index.
    /// </summary>
    /// <value>
    /// The <see cref="Customer" />.
    /// </value>
    /// <param name="index">The index.</param>
    /// <returns>
    /// the Customer from the index
    /// </returns>
    public Customer this[int index]
    {
        get
        {
            return this._customers[index];
        }
        set
        {
            this._customers[index] = value;
        }
    }

    /// <summary>
    /// Gets the <see cref="Customer" /> with the specified identifier.
    /// </summary>
    /// <value>
    /// The <see cref="Customer" />.
    /// </value>
    /// <param name="id">The identifier.</param>
    /// <returns>
    /// the Customer from the id
    /// </returns>
    public Customer this[string id]
    {
        get
        {
            foreach (var c in this._customers)
                if (c.CustomerId == id) return c;
            return null;
        }
    }

    /// <summary>
    /// Gets the customers.
    /// </summary>
    /// <returns>
    /// the customer list
    /// </returns>
    public static CustomerList GetCustomers()
    {
        var listDisplay = (CustomerList)HttpContext.Current.Session["Customer"];
        if (listDisplay == null)
            HttpContext.Current.Session["Customer"] = new CustomerList();
        return (CustomerList)HttpContext.Current.Session["Customer"];
    }

    /// <summary>
    /// Adds the item.
    /// </summary>
    /// <param name="customer"></param>
    public void AddItem(Customer customer)
    {
        this._customers.Add(customer);
    }

    /// <summary>
    /// Removes at.
    /// </summary>
    /// <param name="index">The index.</param>
    public void RemoveAt(int index)
    {
        this._customers.RemoveAt(index);
    }

    /// <summary>
    /// Clears this instance.
    /// </summary>
    public void Clear()
    {
        this._customers.Clear();
    }
}