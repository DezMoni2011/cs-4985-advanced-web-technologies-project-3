﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DigitalManager.master" AutoEventWireup="true" CodeFile="ContactList.aspx.cs" Inherits="ContactList" %>

<asp:Content ID="headSec" ContentPlaceHolderID="headContent" Runat="Server">
    <link href="Styles/Contact.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="TitleSec" ContentPlaceHolderID="bodyTitle" Runat="Server">
    <h2>Contacts List</h2>
</asp:Content>

<asp:Content ID="BodySec" ContentPlaceHolderID="bodyContent" Runat="Server">
     <label id="contactListHeader">Contacts: </label>

    <asp:ListBox ID="lbCustomerList" CssClass="listBox" runat="server"></asp:ListBox>
    <br/>
    <asp:Button ID="btnSelectAdditonalCustomers"  CssClass="button" runat="server" Text="Select Additional Customers" OnClick="btnSelectAdditonalCustomers_Click"  />

    <asp:Button ID="btnRemoveCustomer"  CssClass="button" runat="server" Text="Remove Customer" OnClick="btnRemoveCustomer_Click"/>
    <asp:Button ID="btnClear"  CssClass="button" runat="server" Text="Clear List" OnClick="btnClear_Click"/>
    
</asp:Content>

<asp:Content ID="ErrorMessageSec" ContentPlaceHolderID="bodyErrorMessage" Runat="Server">
    
    <p>
        <asp:Label ID="lblMessage" runat="server" CssClass="error" Enabled="False"></asp:Label>
    </p>
</asp:Content>

