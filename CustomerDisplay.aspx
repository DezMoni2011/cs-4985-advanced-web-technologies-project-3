﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DigitalManager.master" AutoEventWireup="true" CodeFile="CustomerDisplay.aspx.cs" Inherits="CustomerDisplay" %>

<asp:Content ID="headSec" ContentPlaceHolderID="headContent" Runat="Server">
    <link href="Styles/Display.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="TitleSec" ContentPlaceHolderID="bodyTitle" Runat="Server">
    <h2>Customer Information</h2>
</asp:Content>

<asp:Content ID="ContentSec" ContentPlaceHolderID="bodyContent" Runat="Server">
        <label id="customer">Selcet a Customer to add :</label>
        <asp:DropDownList ID="ddlCustomer" CssClass="info" runat="server" AutoPostBack="True"  DataTextField="Name" DataValueField="CustomerID" DataSourceID="sqldsCustomer"></asp:DropDownList>
    <asp:SqlDataSource ID="sqldsCustomer" runat="server" ConnectionString="<%$ ConnectionStrings:strDigitalMananger %>" ProviderName="<%$ ConnectionStrings:strDigitalMananger.ProviderName %>" SelectCommand="SELECT [CustomerID], [Name], [Address], [City], [State], [ZipCode], [Phone], [Email] FROM [Customer] ORDER BY [Name]"></asp:SqlDataSource>

        <label>Customer's ID :</label>
        <asp:Label ID="lblCustomerID" runat="server" Text="" CssClass="dynamicLabel"></asp:Label>
        <label>Address :</label>
        <asp:Label ID="lblAddress" runat="server" Text="" CssClass="em15"></asp:Label>
        <label>City :</label>
        <asp:Label ID="lblCity" runat="server" Text="" CssClass="dynamicLabel"></asp:Label>
        <label id="stateLabel">State :</label>
        <asp:Label ID="lblState" runat="server" Text="" CssClass="em2"></asp:Label>
        <label id="zipCodeLabel">Zip Code :</label>
        <asp:Label ID="lblZipCode" runat="server" Text="" CssClass="em5"></asp:Label>
        <label>Phone Number :</label>
        <asp:Label ID="lblPhone" runat="server" Text="" CssClass="dynamicLabel"></asp:Label>
        <label>E-mail :</label>
        <asp:Label ID="lblEmail" runat="server" Text="" CssClass="em18"></asp:Label>

        <asp:Button ID="btnAddCustomer" runat="server" Text="Add Customer" CssClass="button" OnClick="btnAddCustomer_Click" ></asp:Button>
        <br/>
        <br/>
        <asp:Button ID="btnViewCustomers" runat="server" Text="View Customer List" CssClass="button" OnClick="btnViewCustomers_Click" ></asp:Button>
</asp:Content>

<asp:Content ID="ErrorMessageSec" ContentPlaceHolderID="bodyErrorMessage" Runat="Server">
         <p>
            <asp:Label runat="server" ID="lblErrorMessage" Text="" CssClass="error"></asp:Label>
        </p>
</asp:Content>

