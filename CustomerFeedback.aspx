﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DigitalManager.master" AutoEventWireup="true" CodeFile="CustomerFeedback.aspx.cs" Inherits="CustomerFeedback" %>

<asp:Content ID="headSec" ContentPlaceHolderID="headContent" Runat="Server">
    <link href="Styles/Feedback.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="TitleSec" ContentPlaceHolderID="bodyTitle" Runat="Server">
    <h2>Rate Your Service</h2>
</asp:Content>

<asp:Content ID="ContentSec" ContentPlaceHolderID="bodyContent" Runat="Server">
    <label>Customer ID:</label>
    <asp:TextBox ID="txtCustomerID" runat="server" CausesValidation="True" TabIndex="1"></asp:TextBox>
    <asp:Button ID="btnEnter" runat="server" Text="Enter" OnClick="btnEnter_Click" />
    <asp:Label ID="lblErrorMessage" CssClass="error2" runat="server" Text=""></asp:Label>
    <asp:RequiredFieldValidator ID="rfvCustomerID" runat="server" ErrorMessage="Field is required." Display="Dynamic" ControlToValidate="txtCustomerID"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="cvCustomerId" runat="server" ErrorMessage="Must use Integers" ControlToValidate="txtCustomerID" Display="Dynamic" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
    
    <br/><br/>

    <asp:ListBox ID="lbClosedFeedback" CssClass="listBox" runat="server" AutoPostBack="False" CausesValidation="False"></asp:ListBox>
    <asp:SqlDataSource ID="sqldsClosedFeedback" runat="server" ConnectionString="<%$ ConnectionStrings:strDigitalMananger %>" ProviderName="<%$ ConnectionStrings:strDigitalMananger.ProviderName %>" SelectCommand="SELECT [FeedbackID], [CustomerID], [SoftwareID], [SupportID], [DateOpened], [DateClosed], [Title], [Description] FROM [Feedback]"></asp:SqlDataSource>

    <label id="serviceTimeLabel">Service time:</label>
    <asp:RadioButtonList ID="rdblServiceTime" CssClass="serviceTimeOptions" runat="server">
        <asp:ListItem Value="1">Satisfied</asp:ListItem>
        <asp:ListItem Value="2">Neither Satisfied Nor Dissatisfied </asp:ListItem>
        <asp:ListItem Value="3">Dissatisfied</asp:ListItem>
    </asp:RadioButtonList>
    <asp:RequiredFieldValidator ID="rfvServiceTime" runat="server" 
        ErrorMessage="Must select an option"
        Display="Dynamic"
        ValidationGroup="feedback"
        Text="*"
        ControlToValidate="rdblServiceTime">
    </asp:RequiredFieldValidator>
    
    <label id="probResoulutionLabel">Problem resolution:</label>
    <asp:RadioButtonList ID="rdblResolution" CssClass="probResolutionOptions" runat="server">
        <asp:ListItem Value="1">Satisfied </asp:ListItem>
        <asp:ListItem Value="2">Neither Satisfied Nor Dissatisfied </asp:ListItem>
        <asp:ListItem Value="3">Dissatisfied </asp:ListItem>
    </asp:RadioButtonList>
    <asp:RequiredFieldValidator ID="rfvResolution" runat="server" 
        ErrorMessage="Must select an option"
        Display="Dynamic"
        ValidationGroup="feedback"
        Text="*"
        ControlToValidate="rdblResolution">
    </asp:RequiredFieldValidator>

    <label id="techEfficiencyLabel">Technical efficiency:</label>
    <asp:RadioButtonList ID="rdblEfficieny" CssClass="techEfficiencyOptions" runat="server">
        <asp:ListItem Value="1">Satisfied</asp:ListItem>
        <asp:ListItem Value="2">Neither Satisfied Nor Dissatisfied </asp:ListItem>
        <asp:ListItem Value="3">Dissatisfied </asp:ListItem>
    </asp:RadioButtonList>
    <asp:RequiredFieldValidator ID="rfvEfficiency" runat="server" 
        ErrorMessage="Must select an option"
        ValidationGroup="feedback"
        Display="Dynamic"
        Text="*"
        ControlToValidate="rdblEfficieny">
    </asp:RequiredFieldValidator>
    
     <label id="commentLabel">Additional comments:</label>
    <br/>
    <asp:TextBox ID="txtComments" CssClass="commentBox" runat="server" TextMode="MultiLine"></asp:TextBox>

    <label id="contactLabel">Can we contact you?</label><br/>
    <asp:CheckBox ID="cbContact" CssClass="contactChoice" runat="server" Text="Yes" /><br/><br/>
    
    <label id="methodLabel">If so, how?</label>
    <asp:RadioButtonList ID="rdblPerferredMethod" runat="server" RepeatDirection="Horizontal">
        <asp:ListItem>Phone</asp:ListItem>
        <asp:ListItem>Email</asp:ListItem>
    </asp:RadioButtonList>
    <asp:RequiredFieldValidator ID="rfvPerferredMethod" runat="server" 
        ErrorMessage="Must select an option"
        ValidationGroup="feedback"
        Display="Dynamic"
        Text="*"
        ControlToValidate="rdblPerferredMethod">
    </asp:RequiredFieldValidator>
    <br/>

    <asp:ValidationSummary ID="vsFeedback" runat="server" 
        HeaderText="Please correct the follwing issues."
        ValidationGroup="feedback" />
    <asp:Button ID="btnConfirmFeedback" runat="server" 
        Text="Confirm Feedback"
         OnClick="btnConfirmFeedback_Click" />
</asp:Content>

<asp:Content ID="ErrorMessageSec" ContentPlaceHolderID="bodyErrorMessage" Runat="Server">
    
</asp:Content>


