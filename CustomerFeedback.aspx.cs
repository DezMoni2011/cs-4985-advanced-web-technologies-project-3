﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;

/// <summary>
/// Code behind for the CustomerFeedback page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// February 28, 2015 | Spring
/// </version>
public partial class CustomerFeedback : Page
{
    /// <summary>
    /// The _target feebacks
    /// </summary>
    private List<Feedback> _targetFeebacks;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblErrorMessage.Text = "";
        this.txtCustomerID.Focus();
        this.EnableControls(false);

        if (!IsPostBack)
        {
            return;
        }

        
        this._targetFeebacks = new List<Feedback>();
    }

    /// <summary>
    /// Enables the controls.
    /// </summary>
    /// <param name="willBeEnabled">if set to <c>true</c> [will be enabled].</param>
    private void EnableControls(bool willBeEnabled)
    {
        this.rdblEfficieny.Enabled = willBeEnabled;
        this.rdblResolution.Enabled = willBeEnabled;
        this.rdblServiceTime.Enabled = willBeEnabled;
        this.cbContact.Enabled = willBeEnabled;
        this.txtComments.Enabled = willBeEnabled;
        this.rdblPerferredMethod.Enabled = willBeEnabled;
    }

    /// <summary>
    /// Handles the Click event of the btnEnter control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnEnter_Click(object sender, EventArgs e)
    {

        this.lbClosedFeedback.Items.Clear();

        if (this.txtCustomerID.Text == null)
        {
            return;
        }

        this.ResetControls();
        this.SetFeedback();

        foreach (var feedback in this._targetFeebacks)
        {
            this.lbClosedFeedback.Items.Add(feedback.FormatFeedback());
        }

        
    }

    /// <summary>
    /// Resets the controls.
    /// </summary>
    private void ResetControls()
    {
        this.rdblEfficieny.ClearSelection();
        this.rdblServiceTime.ClearSelection();
        this.rdblResolution.ClearSelection();
        this.rdblPerferredMethod.ClearSelection();
        this.cbContact.Checked = false;
        this.txtComments.Text = "";
    }

    /// <summary>
    /// Gets the feedback.
    /// </summary>
    private void SetFeedback()
    {
        var feedbackTable = (DataView)this.sqldsClosedFeedback.Select(DataSourceSelectArguments.Empty);
        if (feedbackTable == null)
        {
            return;
        }
        

        feedbackTable.RowFilter = string.Format("CustomerID = '{0}'" + "AND DateClosed IS NOT NULL", this.txtCustomerID.Text);

        if (feedbackTable.Count < 1)
        {
            this.lblErrorMessage.Text = "Expressed Id is invalid or has no closed dates currently";
            this.EnableControls(false);
        } 
        else {
            this.EnableControls(true);
            this.lblErrorMessage.Text = "";

            for (var i = 0; i < feedbackTable.Count; i++)
                {
                    var feedback = new Feedback();
                    var row = feedbackTable[i];
                    feedback.SoftwareId = row["SoftwareID"].ToString();
                    feedback.DateClosed = row["DateClosed"].ToString();
                    feedback.Description = row["Description"].ToString();
                    this._targetFeebacks.Add(feedback);
                }
        }
    }
    /// <summary>
    /// Handles the Click event of the btnConfirmFeedback control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnConfirmFeedback_Click(object sender, EventArgs e)
    {
        var description = new Description
        {
            Efficiency = this.rdblEfficieny.SelectedIndex,
            ServiceTime = this.rdblServiceTime.SelectedIndex,
            Resolution = this.rdblResolution.SelectedIndex,
            ContactMethod = this.rdblPerferredMethod.SelectedValue,
            Comments = this.txtComments.Text,
            CustomerId = Convert.ToInt32(this.txtCustomerID.Text),
            Contact = this.cbContact.Checked
        };

        Session["Description"] = description;

        Response.Redirect("FeedbackComplete.aspx");
    }
}