﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DigitalManager.master" AutoEventWireup="true" CodeFile="CustomerMaintenance.aspx.cs" Inherits="CustomerMaintenance" %>

<asp:Content ID="headSec" ContentPlaceHolderID="headContent" Runat="Server">
    <link href="Styles/Maintenance.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="TitleSec" ContentPlaceHolderID="bodyTitle" Runat="Server">
    <h2>Customer Maintainence</h2>
</asp:Content>

<asp:Content ID="ContentSec" ContentPlaceHolderID="bodyContent" Runat="Server">
    <asp:GridView ID="gvMaintenance" runat="server" 
        AutoGenerateColumns="False" 
        DataSourceID="sdsCustomers" 
        AllowSorting="True" 
        AllowPaging="True" 
        PageSize="6"
        DataKeyNames="CustomerID" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <Columns>
            <asp:CommandField ShowSelectButton="True" ButtonType="Button"></asp:CommandField>
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
            <asp:BoundField DataField="City" HeaderText="City" SortExpression="City"></asp:BoundField>
            <asp:BoundField DataField="State" HeaderText="State" SortExpression="State"></asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="sdsCustomers"
        ConnectionString='<%$ ConnectionStrings:strDigitalMananger %>'
        ProviderName='<%$ ConnectionStrings:strDigitalMananger.ProviderName %>'
        SelectCommand="SELECT [Name], [City], [State], [CustomerID] FROM [Customer]" 
        DeleteCommand="DELETE FROM [Customer] WHERE [CustomerID] = ?" 
        InsertCommand="INSERT INTO [Customer] 
            ([Name], [City], [State], [CustomerID]) 
            VALUES (?, ?, ?, ?)" 
        UpdateCommand="UPDATE [Customer] 
            SET [Name] = ?, [City] = ?, [State] = ? 
            WHERE [CustomerID] = ?">
        <DeleteParameters>
            <asp:Parameter Name="CustomerID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Name" Type="String"></asp:Parameter>
            <asp:Parameter Name="City" Type="String"></asp:Parameter>
            <asp:Parameter Name="State" Type="String"></asp:Parameter>
            <asp:Parameter Name="CustomerID" Type="Int32"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String"></asp:Parameter>
            <asp:Parameter Name="City" Type="String"></asp:Parameter>
            <asp:Parameter Name="State" Type="String"></asp:Parameter>
            <asp:Parameter Name="CustomerID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>

    <br />
    <asp:ValidationSummary ID="vsCustomerDetails" runat="server" CssClass="validator" HeaderText="Please correct the following issues." />

    <br />
    <asp:Label ID="lblErrorMessage" runat="server" CssClass="validator"></asp:Label>
    <br />
    <br/>

    <asp:DetailsView ID="dvMaintenance" runat="server"
        Height="50px"
        Width="125px" 
        AutoGenerateRows="False" 
        DataKeyNames="CustomerID" 
        DataSourceID="sdsMaintenanceDetail" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" OnItemDeleted="dvMaintenance_ItemDeleted" OnItemInserted="dvMaintenance_ItemInserted" OnItemUpdated="dvMaintenance_ItemUpdated">

        <AlternatingRowStyle BackColor="#F7F7F7" />
        <EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />

        <Fields>
            <asp:TemplateField HeaderText="CustomerID" InsertVisible="True" SortExpression="CustomerID">
                <EditItemTemplate>
                    <asp:Label runat="server" Text='<%# Eval("CustomerID") %>' ID="Label1"></asp:Label>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtInsertID" runat="server" Text='<%# Bind("CustomerID") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvInsertId" runat="server" ControlToValidate="txtInsertID" CssClass="validator" Display="Dynamic" ErrorMessage="Field is reuired.">*</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvInsertId" runat="server" ControlToValidate="txtInsertID" CssClass="validator" ErrorMessage="Must use numbers." Operator="GreaterThanEqual" Type="Integer" ValueToCompare="0">*</asp:CompareValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Bind("CustomerID") %>' ID="Label8"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Name" SortExpression="Name">
                <EditItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("Name") %>' ID="txtName"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" CssClass="validator" Display="Dynamic" ErrorMessage="Name is required.">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("Name") %>' ID="txtInsertName"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvinsertName" runat="server" ControlToValidate="txtInsertName" CssClass="validator" Display="Dynamic" ErrorMessage="Field is required.">*</asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Bind("Name") %>' ID="Label1"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address" SortExpression="Address">
                <EditItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("Address") %>' ID="txtAddress"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress" CssClass="validator" Display="Dynamic" ErrorMessage="Address is required.">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("Address") %>' ID="txtInsertAddress"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvINsertAddress" runat="server" ControlToValidate="txtInsertAddress" CssClass="validator" Display="Dynamic" ErrorMessage="Field is required.">*</asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Bind("Address") %>' ID="Label2"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="City" SortExpression="City">
                <EditItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("City") %>' ID="txtCity"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity" CssClass="validator" Display="Dynamic" ErrorMessage="City is required.">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("City") %>' ID="txtInsertCity"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvInsertCity" runat="server" ControlToValidate="txtInsertCity" CssClass="validator" Display="Dynamic" ErrorMessage="Field is required.">*</asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Bind("City") %>' ID="Label3"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="State" SortExpression="State">
                <EditItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("State") %>' ID="txtState" MaxLength="2"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvState" runat="server" ControlToValidate="txtState" CssClass="validator" Display="Dynamic" ErrorMessage="State is required.">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("State") %>' ID="txtInsertState" MaxLength="2"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvInsertState" runat="server" ControlToValidate="txtInsertState" CssClass="validator" Display="Dynamic" ErrorMessage="Field is required.">*</asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Bind("State") %>' ID="Label4"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ZipCode" SortExpression="ZipCode">
                <EditItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("ZipCode") %>' ID="txtZip" MaxLength="5"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvZip" runat="server" ControlToValidate="txtZip" CssClass="validator" Display="Dynamic" ErrorMessage="Zip code is required.">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rev" runat="server" ControlToValidate="txtZip" CssClass="validator" Display="Dynamic" ErrorMessage="Please use valid zip code." ValidationExpression="\d{5}(-\d{4})?">*</asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("ZipCode") %>' ID="txtInsertZip" MaxLength="5"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revInsertZip" runat="server" ControlToValidate="txtInsertZip" CssClass="validator" Display="Dynamic" ErrorMessage="Must be a  valid zip code." ValidationExpression="\d{5}(-\d{4})?">*</asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvInsertZip" runat="server" ControlToValidate="txtInsertZip" CssClass="validator" Display="Dynamic" ErrorMessage="Field is required.">*</asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Bind("ZipCode") %>' ID="Label5"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Phone" SortExpression="Phone">
                <EditItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("Phone") %>' ID="txtPhone"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="txtPhone" CssClass="validator" Display="Dynamic" ErrorMessage="Phone number is required.">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revPhone" runat="server" ControlToValidate="txtPhone" CssClass="validator" Display="Dynamic" ErrorMessage="Phone number should be in the proper format : 888-888-8888." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("Phone") %>' ID="txtInsertPhone"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvInsertPhone" runat="server" ControlToValidate="txtInsertPhone" CssClass="validator" Display="Dynamic" ErrorMessage="Field is required.">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revInsertPhone" runat="server" ControlToValidate="txtInsertPhone" CssClass="validator" Display="Dynamic" ErrorMessage="Must be proper format : 888-888-8888." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Bind("Phone") %>' ID="Label6"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email" SortExpression="Email">
                <EditItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("Email") %>' ID="txtEmail"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" CssClass="validator" Display="Dynamic" ErrorMessage="Email is required.">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" CssClass="validator" Display="Dynamic" ErrorMessage="Email must be properly formated : email@host.com" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox runat="server" Text='<%# Bind("Email") %>' ID="txtInsertEmail"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvInsertEmail" runat="server" ControlToValidate="txtInsertEmail" CssClass="validator" Display="Dynamic" ErrorMessage="Field is required.">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revInsertEmail" runat="server" ControlToValidate="txtInsertEmail" CssClass="validator" Display="Dynamic" ErrorMessage="Must be proper foramt : email@host.com" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label runat="server" Text='<%# Bind("Email") %>' ID="Label7"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:CommandField ShowInsertButton="True" ShowEditButton="True" ShowDeleteButton="True" ButtonType="Button"></asp:CommandField>
        </Fields>
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
    </asp:DetailsView>
    <asp:SqlDataSource runat="server" ID="sdsMaintenanceDetail" 
        ConnectionString='<%$ ConnectionStrings:strDigitalMananger %>' 
        DeleteCommand="DELETE FROM [Customer] WHERE [CustomerID] = ?" 
        InsertCommand="INSERT INTO [Customer] 
            ([CustomerID], [Name], [Address], [City], [State], [ZipCode], [Phone], [Email]) 
        VALUES (?, ?, ?, ?, ?, ?, ?, ?)" 
        ProviderName='<%$ ConnectionStrings:strDigitalMananger.ProviderName %>'
         SelectCommand="SELECT * FROM [Customer] WHERE ([CustomerID] = ?)" 
        UpdateCommand="UPDATE [Customer] 
            SET [Name] = ?, [Address] = ?, [City] = ?, [State] = ?, [ZipCode] = ?, 
                [Phone] = ?, [Email] = ? 
            WHERE [CustomerID] = ?">
        <DeleteParameters>
            <asp:Parameter Name="CustomerID" Type="Int32"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="CustomerID" Type="Int32"></asp:Parameter>
            <asp:Parameter Name="Name" Type="String"></asp:Parameter>
            <asp:Parameter Name="Address" Type="String"></asp:Parameter>
            <asp:Parameter Name="City" Type="String"></asp:Parameter>
            <asp:Parameter Name="State" Type="String"></asp:Parameter>
            <asp:Parameter Name="ZipCode" Type="String"></asp:Parameter>
            <asp:Parameter Name="Phone" Type="String"></asp:Parameter>
            <asp:Parameter Name="Email" Type="String"></asp:Parameter>
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="gvMaintenance" 
                PropertyName="SelectedValue" 
                Name="CustomerID" 
                Type="Int32">
            </asp:ControlParameter>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String"></asp:Parameter>
            <asp:Parameter Name="Address" Type="String"></asp:Parameter>
            <asp:Parameter Name="City" Type="String"></asp:Parameter>
            <asp:Parameter Name="State" Type="String"></asp:Parameter>
            <asp:Parameter Name="ZipCode" Type="String"></asp:Parameter>
            <asp:Parameter Name="Phone" Type="String"></asp:Parameter>
            <asp:Parameter Name="Email" Type="String"></asp:Parameter>
            <asp:Parameter Name="CustomerID" Type="Int32"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

