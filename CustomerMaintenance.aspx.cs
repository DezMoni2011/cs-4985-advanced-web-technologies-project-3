﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code behind for the CustomerMaintenance page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// April 5, 2015 | Spring
/// </version>
public partial class CustomerMaintenance : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblErrorMessage.Text = "";
    }
    /// <summary>
    /// Handles the ItemDeleted event of the dvMaintenance control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DetailsViewDeletedEventArgs"/> instance containing the event data.</param>
    protected void dvMaintenance_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblErrorMessage.Text = "Database issue has occurred.<br/ ><br/ >" + "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
        }
    }
    /// <summary>
    /// Handles the ItemInserted event of the dvMaintenance control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DetailsViewInsertedEventArgs"/> instance containing the event data.</param>
    protected void dvMaintenance_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblErrorMessage.Text = "Database issue has occurred.<br/ ><br/ >" + "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInInsertMode = true;
        }
    }
    /// <summary>
    /// Handles the ItemUpdated event of the dvMaintenance control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DetailsViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void dvMaintenance_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblErrorMessage.Text = "Database issue has occurred.<br/ ><br/ >" + "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
    }
}