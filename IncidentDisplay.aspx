﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DigitalManager.master" AutoEventWireup="true" CodeFile="IncidentDisplay.aspx.cs" Inherits="IncidentDisplay" %>

<asp:Content ID="headSec" ContentPlaceHolderID="headContent" Runat="Server">
    <link href="Styles/Incident.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="TitleSec" ContentPlaceHolderID="bodyTitle" Runat="Server">
    <h2>Existing Incidents</h2>
</asp:Content>

<asp:Content ID="ContentSec" ContentPlaceHolderID="bodyContent" Runat="Server">
    <label id="customerLabel">Choose a customer: </label>
    <asp:DropDownList ID="ddlCustomers" runat="server" DataSourceID="sdsCustomers" DataTextField="Name" DataValueField="CustomerID" AutoPostBack="True"></asp:DropDownList>
    <asp:SqlDataSource runat="server" ID="sdsCustomers" ConnectionString='<%$ ConnectionStrings:strDigitalMananger %>' ProviderName='<%$ ConnectionStrings:strDigitalMananger.ProviderName %>' SelectCommand="SELECT [Name], [CustomerID] FROM [Customer] ORDER BY [Name]"></asp:SqlDataSource>
    
    <h3><asp:Label ID="lblCustomer" runat="server" Text="Customer"></asp:Label>'s incidents on file:</h3>

    <asp:DataList ID="dlIncidents" runat="server" 
        DataSourceID="sdsIncidents" 
        CellPadding="3" 
        BackColor="White" 
        BorderColor="#E7E7FF" 
        BorderStyle="None" 
        BorderWidth="1px" 
        GridLines="Horizontal">
        <AlternatingItemStyle BackColor="#F7F7F7" />
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <HeaderTemplate>
            <table>
                 <tr>
                  <td>
                      Software/Incident:
                  </td> 
                    <td>
                      Technician Name:
                  </td>
                    <td>
                      Date Opened:
                  </td> 
                    <td>
                       Date Closed:
                  </td>   
                </tr>
            </table>
        </HeaderTemplate>
        <ItemStyle BackColor="#E7E7FF" ForeColor="#4A3C8C"/>
        <ItemTemplate>
            <table>
                <tr>
                  <td>
                      <asp:Label Text='<%# Eval("[Software/Incident]") %>' runat="server" ID="Software_IncidentLabel" />                    
                  </td> 
                    <td>
                      <asp:Label Text='<%# Eval("[Technician Name]") %>' runat="server" ID="Technician_NameLabel" />
                  </td>
                    <td>
                      <asp:Label Text='<%# Eval("[Date Opened]") %>' runat="server" ID="Date_OpenedLabel" />
                  </td> 
                    <td>
                      <asp:Label Text='<%# Eval("[Date Closed]") %>' runat="server" ID="Date_ClosedLabel" />
                  </td>   
                </tr>
                <tr>
                    <td colspan="4">
                      <asp:Label Text='<%# Eval("Description") %>' runat="server" ID="DescriptionLabel" />
                  </td> 
                </tr>
            </table>
        </ItemTemplate>
        <SelectedItemStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
    </asp:DataList>

    <asp:SqlDataSource runat="server" ID="sdsIncidents"
        ConnectionString='<%$ ConnectionStrings:strDigitalMananger %>'
        ProviderName='<%$ ConnectionStrings:strDigitalMananger.ProviderName %>'
        SelectCommand="SELECT Software.Name AS [Software/Incident], Support.Name AS [Technician Name], Feedback.DateOpened AS [Date Opened], Feedback.DateClosed AS [Date Closed], Feedback.Description FROM ((Software INNER JOIN Feedback ON Software.SoftwareID = Feedback.SoftwareID) INNER JOIN Support ON Feedback.SupportID = Support.SupportID) WHERE (Feedback.CustomerID = ?)">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlCustomers" Name="?" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>

    <br />

    <asp:Button ID="btnViewSoftware" runat="server" Text="View Software" CssClass="button" OnClick="btnViewSoftware_Click" />
    <br />
    <br/>
    <asp:Button ID="btnManageCustomers" runat="server" Text="Manage Customers" CssClass="button" OnClick="btnManageCustomers_Click" />
</asp:Content>

<asp:Content ID="ErrorMessageSec" ContentPlaceHolderID="bodyErrorMessage" Runat="Server">
    <br />
    <br />
    <asp:Label ID="lblErrorMessage" CssClass="error" runat="server"></asp:Label> 
</asp:Content>

