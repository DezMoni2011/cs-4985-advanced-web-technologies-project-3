﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DigitalManager.master" AutoEventWireup="true" CodeFile="Products.aspx.cs" Inherits="Products" %>

<asp:Content ID="headSec" ContentPlaceHolderID="headContent" Runat="Server">
    <link href="Styles/Product.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="TitleSec" ContentPlaceHolderID="bodyTitle" Runat="Server">
    <h2>Software Maintenance</h2>
</asp:Content>

<asp:Content ID="ContentSec" ContentPlaceHolderID="bodyContent" Runat="Server">
    <asp:GridView ID="gvSoftware" runat="server" 
        AutoGenerateColumns="False" 
        DataKeyNames="SoftwareID" 
        DataSourceID="sdsSoftware" 
        OnRowUpdated="gvSoftware_RowUpdated" 
        OnRowDeleted="gvSoftware_RowDeleted" 
        BackColor="White" 
        BorderColor="#E7E7FF" 
        BorderStyle="None" 
        BorderWidth="1px" 
        CellPadding="3" 
        GridLines="Horizontal" 
        Width="643px">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <Columns>
            <asp:BoundField DataField="SoftwareID" 
                            HeaderText="Software ID" 
                            ReadOnly="True" 
                            SortExpression="SoftwareID">
            </asp:BoundField>
            <asp:TemplateField HeaderText="Name" SortExpression="Name">
                <EditItemTemplate>
                    <asp:TextBox ID="txtEditName" runat="server" 
                        Text='<%# Bind("Name") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEditName" runat="server" 
                        ControlToValidate="txtEditName" 
                        CssClass="validator2" 
                        Display="Dynamic" 
                        ErrorMessage="Field is required" 
                        ValidationGroup="grid">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" 
                                Text='<%# Bind("Name") %>'>
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Version" SortExpression="Version">
                <EditItemTemplate>
                    <asp:TextBox ID="txtEditVersion" runat="server" 
                        Text='<%# Bind("Version") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEditVersion" runat="server" 
                        ControlToValidate="txtEditVersion" 
                        CssClass="validator2" 
                        Display="Dynamic" 
                        ErrorMessage="Field is required." 
                        ValidationGroup="grid">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" 
                        Text='<%# Bind("Version") %>'>
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ReleaseDate" SortExpression="ReleaseDate">
                <EditItemTemplate>
                    <asp:TextBox ID="txtEditDate" runat="server" 
                        Text='<%# Bind("ReleaseDate") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEditDate" runat="server" 
                        ControlToValidate="txtEditDate" 
                        CssClass="validator2" 
                        Display="Dynamic" 
                        ErrorMessage="Field is required." 
                        ValidationGroup="grid">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("ReleaseDate") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" ValidationGroup="grid" />
            <asp:CommandField ShowDeleteButton="True" />
        </Columns>
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />
    </asp:GridView>
    <asp:SqlDataSource runat="server" ID="sdsSoftware"
        ConnectionString='<%$ ConnectionStrings:strDigitalMananger %>'
        ProviderName='<%$ ConnectionStrings:strDigitalMananger.ProviderName %>'
        SelectCommand="SELECT * FROM [Software] ORDER BY [SoftwareID]" 
        DeleteCommand="DELETE FROM [Software] WHERE [SoftwareID] = ?" 
        InsertCommand="INSERT INTO [Software] 
            ([SoftwareID], [Name], [Version], [ReleaseDate]) 
            VALUES (?, ?, ?, ?)" 
        UpdateCommand="UPDATE [Software] 
            SET [Name] = ?, [Version] = ?, [ReleaseDate] = ? 
            WHERE [SoftwareID] = ?">
        <DeleteParameters>
            <asp:Parameter Name="SoftwareID" Type="String"></asp:Parameter>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="SoftwareID" Type="String"></asp:Parameter>
            <asp:Parameter Name="Name" Type="String"></asp:Parameter>
            <asp:Parameter Name="Version" Type="Decimal"></asp:Parameter>
            <asp:Parameter Name="ReleaseDate" Type="DateTime"></asp:Parameter>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String"></asp:Parameter>
            <asp:Parameter Name="Version" Type="Decimal"></asp:Parameter>
            <asp:Parameter Name="ReleaseDate" Type="DateTime"></asp:Parameter>
            <asp:Parameter Name="SoftwareID" Type="String"></asp:Parameter>
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

<asp:Content ID="ErrorMessageSec" ContentPlaceHolderID="bodyErrorMessage" Runat="Server">

    <asp:Label ID="lblErrorMessage" runat="server" CssClass="validator"></asp:Label>

    <asp:ValidationSummary ID="vsSoftware" runat="server" 
        HeaderText="Please correct the following errors." CssClass="validator" ValidationGroup="grid" />

    <p>To create a new category, enter the category information and click Add New Software.</p>
        <label>ID:</label>
        <asp:TextBox ID="txtID" runat="server" 
                CssClass="entry">
        </asp:TextBox>&nbsp;
            <asp:RequiredFieldValidator ID="rfvID" 
                runat="server" 
                ControlToValidate="txtID" 
                CssClass="validator"  
                ErrorMessage="ID is a required field." 
                Display="Dynamic">
            </asp:RequiredFieldValidator><br />
        <label>Name:</label>
        <asp:TextBox ID="txtName" runat="server" 
                CssClass="entry">
        </asp:TextBox>&nbsp;
            <asp:RequiredFieldValidator ID="rfvName" 
                runat="server" ControlToValidate="txtName" 
                CssClass="validator" 
                ErrorMessage="Name is a required field."></asp:RequiredFieldValidator><br />
        <label>Version:</label>
        <asp:TextBox ID="txtVersion" runat="server" CssClass="entry">
        </asp:TextBox>&nbsp;
            <asp:RequiredFieldValidator ID="rfvVersion" 
                runat="server" ControlToValidate="txtVersion" 
                CssClass="validator"
                ErrorMessage="Version is a required field."></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="cvVersion" runat="server" 
                ErrorMessage="Must be a number" 
                ValueToCompare="0" 
                Operator="GreaterThanEqual" 
                ControlToValidate="txtVersion" 
                Type="Double" 
                Display="Dynamic" 
                CssClass="validator"></asp:CompareValidator><br />
     <label>Relese Date:</label>
        <asp:TextBox ID="txtDate" runat="server" CssClass="entry">
        </asp:TextBox>&nbsp;
            <asp:RequiredFieldValidator ID="rfvDate" 
                runat="server" ControlToValidate="txtDate" CssClass="validator"
                ErrorMessage="Date is a required field."></asp:RequiredFieldValidator>   
    <asp:RegularExpressionValidator ID="revDate" runat="server" 
                ControlToValidate="txtDate" 
                CssClass="validator" 
                Display="Dynamic" 
                ErrorMessage="Date format must match mm/dd/yyyy" 
                ValidationExpression="(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$"></asp:RegularExpressionValidator>
    <br />
    <asp:Button ID="btnAdd" runat="server" Text="Add New Software" OnClick="btnAdd_Click" 
            />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CausesValidation="False" />
</asp:Content>

