﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code behind for the Products page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// April 4, 2015 | Spring
/// </version>
public partial class Products : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    /// <summary>
    /// Handles the RowUpdated event of the gvSoftware control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewUpdatedEventArgs" /> instance containing the event data.</param>
    protected void gvSoftware_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblErrorMessage.Text = "Database issue has occurred.<br/ ><br/ >" + "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else if(e.KeepInEditMode == false)
        {
            this.lblErrorMessage.Text = "";
        }
    }
    /// <summary>
    /// Handles the Click event of the btnAdd control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        

        this.sdsSoftware.InsertParameters["SoftwareID"].DefaultValue
            = this.txtID.Text;
        this.sdsSoftware.InsertParameters["Name"].DefaultValue
            = this.txtName.Text;
        this.sdsSoftware.InsertParameters["Version"].DefaultValue
            = this.txtVersion.Text;
        this.sdsSoftware.InsertParameters["ReleaseDate"].DefaultValue = this.txtDate.Text;

       
        try
        {
            this.sdsSoftware.Insert();
            this.txtID.Text = "";
            this.txtName.Text = "";
            this.txtVersion.Text = "";
            this.txtDate.Text = "";
        }
        catch (Exception ex)
        {
            this.lblErrorMessage.Text = "A database error has occurred.<br /><br />" +
                "Message: " + ex.Message;

            
        }

    }
    /// <summary>
    /// Handles the RowDeleted event of the gvSoftware control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewDeletedEventArgs" /> instance containing the event data.</param>
    protected void gvSoftware_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblErrorMessage.Text = "Database issue has occurred.<br/ ><br/ >" + "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
        }
    }
    /// <summary>
    /// Handles the Click event of the btnCancel control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        this.lblErrorMessage.Text = "";
        this.txtID.Text = "";
        this.txtName.Text = "";
        this.txtVersion.Text = "";
        this.txtDate.Text = "";
    }
}