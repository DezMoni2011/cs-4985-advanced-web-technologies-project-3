﻿using System;
using System.Web.UI;

/// <summary>
/// Code behind for the SiteMape page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// March 4, 2015 | Spring
/// </version>
public partial class SiteMap : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}